module gitlab.wikimedia.org/repos/releng/cli

require (
	cgt.name/pkg/go-mwclient v1.2.0
	github.com/AlecAivazis/survey/v2 v2.3.2
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/andygrunwald/go-gerrit v0.0.0-20210919125110-ff14d0674afb
	github.com/antonholmquist/jason v1.0.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/briandowns/spinner v1.18.1 // indirect
	github.com/charmbracelet/glamour v0.4.0
	github.com/cli/cli/v2 v2.5.1
	github.com/containerd/containerd v1.5.7 // indirect
	github.com/docker/docker v20.10.9+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/fatih/color v1.13.0
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/hashicorp/go-version v1.3.0
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/itchyny/gojq v0.12.6
	github.com/joho/godotenv v1.4.0
	github.com/mattn/go-isatty v0.0.14
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.4.3
	github.com/moby/term v0.0.0-20201216013528-df9cb8a40635 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/mrjones/oauth v0.0.0-20190623134757-126b35219450 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/profclems/glab v1.22.0
	github.com/rhysd/go-github-selfupdate v1.2.3
	github.com/rodaine/table v1.0.1
	github.com/schollz/progressbar/v3 v3.8.5
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	github.com/txn2/txeh v1.3.0
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/xanzy/go-gitlab v0.51.1
	golang.org/x/crypto v0.0.0-20220126234351-aa10faf2a1f8 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/ini.v1 v1.66.2
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

go 1.16
